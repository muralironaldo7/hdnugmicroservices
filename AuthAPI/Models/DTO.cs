﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthAPI.Models
{
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponse
    {
        public string Username { get; set; }
        public string SessionToken { get; set; }
        public DateTime ExpiresOn { get; set; }
        public ErrorSegment error { get; set; }

        public LoginResponse()
        {
            this.error = new ErrorSegment();
        }
    }

    public class VerifyRequest
    {
        public string UserName { get; set; }
        public string SessionToken { get; set; }
    }

    public class ErrorSegment
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public static class secAgent
    {
        public static string Crypt(string text)
        {
            byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            return System.Convert.ToBase64String(data);
        }

        public static string Derypt(this string text)
        {
            byte[] data = System.Convert.FromBase64String(text);
            return System.Text.ASCIIEncoding.ASCII.GetString(data);
        }
    }
}
