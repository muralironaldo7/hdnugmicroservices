﻿using Microsoft.AspNetCore.Mvc;
using System;
using MongoDB;
using AuthAPI.Services;
using AuthAPI.Models;

namespace AuthAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UsersService _usersService;

        public AuthController(UsersService usersService)
        {
            _usersService = usersService;
        }

        // POST api/Auth/Login
        [HttpPost("Login")]
        public ActionResult<LoginResponse> Login([FromBody] LoginRequest request)
        {
            LoginResponse response = new LoginResponse();

            try
            {
                if (request != null)
                {
                    if (request.Username != null && request.Password != null)
                    {
                        //Connect to DB
                            User u = _usersService.Get(request.Username);
                        if (u != null)
                        {
                            if (u.password.Equals(request.Password))
                            {
                                DateTime expDate = DateTime.Now.AddHours(1);
                                response.SessionToken = secAgent.Crypt("testApp|" + request.Username + "|" + expDate.ToString());
                                response.ExpiresOn = expDate;
                            }
                            else
                            {
                                response.error.ErrorCode = -2;
                                response.error.ErrorMessage = "Unable to authenticate user. Please check username & password combination";
                                return Unauthorized(response);
                            }
                        }
                        else
                        {
                            response.error.ErrorCode = -1;
                            response.error.ErrorMessage = "Unable to authenticate user. Please check username & password combination";
                            return Unauthorized(response);
                        }

                        response.Username = request.Username;
                        return Ok(response);
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch(Exception e)
            {
                response.error.ErrorCode = -10;
                response.error.ErrorMessage = e.Message;
                return StatusCode(500, response);
            }
            
        }

        // POST api/Auth/Logout
        [HttpPost("Logout")]
        public ActionResult<LoginResponse> Logout([FromBody] LoginRequest request)
        {
            LoginResponse response = new LoginResponse();

            if (request != null)
            {
                if (request.Username != null && request.Password != null)
                {
                    //Connect to DB
                    //Get Session Token
                    return Ok(response);
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        // POST api/Auth/Verfy
        [HttpPost("Verify")]
        public ActionResult Verify([FromBody] VerifyRequest request)
        {
            if (request != null)
            {
                if (request.UserName != null && request.SessionToken != null)
                {
                    string decode = secAgent.Derypt(request.SessionToken);
                    string[] sParts = decode.Split("|");
                    if (sParts.Length > 0)
                    {
                        if (sParts[0] != "testApp" || sParts[1].ToLower() != request.UserName.ToLower())
                        {
                            return Unauthorized();
                        }
                        else if (DateTime.Parse(sParts[2]) < DateTime.Now)
                        {
                            return Unauthorized();
                        }
                        else
                        {
                            return Ok();
                        }
                    }
                    return Unauthorized();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}