﻿using MongoDB.Driver;
using AuthAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;

namespace AuthAPI.Services
{
    public class UsersService
    {
     
        private readonly IMongoCollection<User> _users;
        public UsersService(IDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<User>(settings.CollectionName);

        }

        public List<User> Get()
        {
            List<User> users;
            users = _users.Find(emp => true).ToList();
            return users;
        }

        public User Get(string username) =>
            _users.Find<User>(u => u.username == username).FirstOrDefault();

        
    }
}
