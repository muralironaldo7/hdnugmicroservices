﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AccountAPI.Models;
using AccountAPI.Services;
using System.Net;
using System.Collections.Specialized;
using System.Text;

namespace AccountAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly AccountsService _accountService;
        public AccountsController(AccountsService accountService)
        {
            _accountService = accountService;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Account>> Get()
        {
            if(Request.Headers.ContainsKey("SessionToken") && Request.Headers.ContainsKey("Username"))
            {
                //Should be implemented in Middleware - Best practice 
                if(Verify(Request.Headers["Username"].ToString(), Request.Headers["SessionToken"].ToString()))
                {
                    var result = _accountService.Get();
                    if (result != null)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return NoContent();
                    }
                }
                else
                {
                    return Unauthorized();
                }
                
            }
            else
            {
                return Unauthorized();
            }
            
        }

        private bool Verify(string username,string token)
        {
            bool result = false;

            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["username"] = username;
                data["SessionToken"] = token;
                wb.Headers["ContentType"] = "application/json";

                var response = wb.UploadValues("http://localhost:56629/api/Auth/Verify", "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);
            }


            return result;
        }
    }
}
