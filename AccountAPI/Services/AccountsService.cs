﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using AccountAPI.Models;

namespace AccountAPI.Services
{
    public class AccountsService
    {
        private readonly IMongoCollection<Account> _accounts;

        public AccountsService(IDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _accounts = database.GetCollection<Account>(settings.CollectionName);

        }

        public List<Account> Get()
        {
            List<Account> accounts;
            accounts = _accounts.Find(emp => true).ToList();
            return accounts;
        }

        public Account Get(string accnumber) =>
            _accounts.Find<Account>(a => a.number == accnumber).FirstOrDefault();
    }
}
