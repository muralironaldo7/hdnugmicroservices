﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogisticsAPI.Models
{
    public class MailLabelRequest
    {
        public string Address { get; set; }
        public int weight { get; set; }
        public bool PriorityShipping { get; set; }
    }

    public class MailLabelResponse
    {
        public double Cost { get; set; }
        public string TrackingNumber { get; set; }
        public DateTime EstDeliveryDate { get; set; }
    }

    public class TrackingRequest
    {
        public string TrackingNumber { get; set; }
    }

    public class TrackingResponse
    {
        public List<TrackingDetail> TrackingHistory { get; set; }
        public bool Delivered { get; set; }

        public TrackingResponse()
        {
            TrackingHistory = new List<TrackingDetail>();
        }
    }

    public class TrackingDetail
    {
        public DateTime ItemDate { get; set; }
        public string Description { get; set; }

        public TrackingDetail(DateTime d, string desc)
        {
            this.ItemDate = d;
            this.Description = desc;
        }

    }
}
