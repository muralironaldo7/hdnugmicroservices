﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LogisticsAPI.Models;

namespace LogisticsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        
        [HttpPost("Label")]
        public ActionResult<MailLabelResponse> CreateLabel([FromBody] MailLabelRequest request)
        {
            MailLabelResponse response = new MailLabelResponse();

            double _cost = 0.0;
            if(request.weight < 3)
            {
                _cost = 10.25;
            }
            else if(request.weight >3 && request.weight < 7)
            {
                _cost = 15.00;
            }
            else if (request.weight >= 7)
            {
                _cost = 20.00;
            }

            if(request.PriorityShipping)
            {
                _cost = Math.Round(_cost * 1.2,2);
            }

            response.Cost = _cost;
            response.EstDeliveryDate = request.PriorityShipping ? DateTime.Now.AddDays(1) : DateTime.Now.AddDays(3);
            response.TrackingNumber = "1Z54F78A0450293517";

            return Ok(response);
        }

        [HttpPost("Track")]
        public ActionResult<TrackingResponse> Track([FromBody] TrackingRequest request)
        {
            TrackingResponse response = new TrackingResponse();

            response.Delivered = false;
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddDays(-2),"Shipping info received"));
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddDays(-2).AddHours(4), "Item left warehouse, San Jose, CA"));
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddDays(-1), "Reached sorting facility, Houston, TX"));
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddDays(-1).AddHours(6), "Departed sorting facility, Houston, TX"));
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddDays(-1).AddHours(12), "Reached destination post office, Spring TX"));
            response.TrackingHistory.Add(new TrackingDetail(DateTime.Now.AddHours(-3), "Out for delivery"));

            return Ok(response);
        }

    }
}
